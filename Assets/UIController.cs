﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

	public static UIController inst;
	
	public GameObject buildPanel;
	public GameObject outOfRangeWarning;
	public GameObject uiBackground;
	public GameObject inGameUI;
	public GameObject victoryScreen;
	public GameObject gameOverScreen;
	//buildPanel;
	//outOfRangeWarning;
	//uiBackground;
	//victoryScreen;
	//gameOverScreen
	private bool _ingameMenuActive = false;
	public bool ingameMenuActive {
		get {
			return _ingameMenuActive;
		}
		set {
			_ingameMenuActive = value;
			if (value) {
				OpenIngameUI();
			}
			else {
				CloseIngameUI();
			}
		}
	}
	
	void Awake() {
		inst = this;
	}
	
	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			ingameMenuActive = !ingameMenuActive;
		}
	}
	
	public void OpenDefeatUI() {
		_ingameMenuActive = false;
		inGameUI.active = false;
		buildPanel.active = false;
		outOfRangeWarning.active = false;
		uiBackground.active = true;
		victoryScreen.active = false;
		gameOverScreen.active = true;
	}
	
	public void OpenVictoryUI() {
		_ingameMenuActive = false;
		inGameUI.active = false;
		buildPanel.active = false;
		outOfRangeWarning.active = false;
		uiBackground.active = true;
		victoryScreen.active = true;
		gameOverScreen.active = false;
	}
	
	public void OpenIngameUI() {
		_ingameMenuActive = true;
		Time.timeScale = 0;
		inGameUI.active = true;
		buildPanel.active = false;
		outOfRangeWarning.active = false;
		uiBackground.active = true;
		victoryScreen.active = false;
		gameOverScreen.active = false;
	}
	
	
	public void CloseIngameUI() {
		_ingameMenuActive = false;
		Time.timeScale = 1;
		inGameUI.active = false;
		buildPanel.active = true;
		outOfRangeWarning.active = false;
		uiBackground.active = false;
		victoryScreen.active = false;
		gameOverScreen.active = false;
	}
}
