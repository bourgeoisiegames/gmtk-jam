﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserImpact : MonoBehaviour {

	public float duration = 1.0f;
	public float _timeLeft = 0f;

	// Use this for initialization
	void Start () {
		_timeLeft = duration; 
	}
	
	// Update is called once per frame
	void Update () {
		_timeLeft -= Time.deltaTime;
		if (_timeLeft <= 0) {
			Destroy(gameObject);
		}
	}
}
