﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class construction_site : MonoBehaviour {

	public float build_time = 10.0f;
	public Transform to_build = null;

	private float finished_at;

	// Use this for initialization
	void Start () {
		finished_at = Time.time + build_time;	}

	// Update is called once per frame
	void Update () {
		if(Time.time > finished_at){
			Instantiate(to_build, transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}


}
