﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWarningCtrl : MonoBehaviour {

	public GameObject warningMessage;
	public float warningDurationSeconds = 4.0f;
	private float _timeAlive;
	
	public static RangeWarningCtrl inst;
	
	void Awake() {
		inst = this;
	}
	
	void Start() {
		warningMessage.active = false;
		_timeAlive = 0;
	}
	
	public void ShowMessage() {
		_timeAlive = warningDurationSeconds;
		warningMessage.active = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (_timeAlive <= 0) {
			warningMessage.active = false;
		}
		else {
			_timeAlive -= Time.deltaTime;
		}
	}
}
