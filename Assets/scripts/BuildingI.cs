﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface BuildingI {
	
	float currentHp { get; set;}
	
	void DealDamage(float damage);
	
}
