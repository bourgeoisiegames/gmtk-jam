﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTurret : MonoBehaviour {

	public float turnSpeed;
	public Transform moveableTurret;
	public GameObject gunObj; 
	private TowerGun gunScript;    

	// Use this for initialization
	void Start () {
		gunScript = gunObj.GetComponent<TowerGun>();
	}
	
	// Update is called once per frame
	void Update () {
		if (gunScript.currentTarget == null) {
			Debug.Log("no target");
		}
		else if (IsFacingTarget(gunScript.currentTarget)) {
			Debug.Log("facing target, open fire!");
			gunScript.attacking = true;
		}
		else {
			Debug.Log("turning to face target...");
			gunScript.attacking = false; // TODO change this
			FaceTarget(gunScript.currentTarget);
		}
//		HeadSpin();
	}

	public void HeadSpin() {
		Vector3 newRot = moveableTurret.transform.rotation.eulerAngles + new Vector3(0, Time.deltaTime * 10, 0);
		moveableTurret.transform.rotation = Quaternion.Euler(newRot);
	}
	
	public void FaceTarget(Transform target) {
//		Quaternion lookRotation = GetLookRotation(target);
		Quaternion lookRotation = GetLookYAxis(target);
        moveableTurret.rotation = Quaternion.Slerp(moveableTurret.rotation, lookRotation, (Time.deltaTime * turnSpeed));
	}
	
	// Gets "lookRotation" quaterneon, then restricts it to only rotate the Y axis
	public Quaternion GetLookYAxis(Transform target) {
		Vector3 look = GetLookRotation(target).eulerAngles;
		Vector3 rot = moveableTurret.rotation.eulerAngles;
		Vector3 lookY = new Vector3(rot.x, look.y, rot.z);
		return Quaternion.Euler(lookY);
	}
	
	
	public Quaternion GetLookRotation(Transform target) {
        //find the vector pointing from our position to the target
        Vector3 direction = (target.position - moveableTurret.position).normalized;
        //create the rotation we need to be in to look at the target
        return Quaternion.LookRotation(direction);
	}
	
	public bool IsFacingTarget(Transform target) {
		return IsFacingTarget(target, .1f);
	}
	
	public bool IsFacingTarget(Transform target, float threshold) {
		Vector3 lookRotation = GetLookRotation(target).eulerAngles;
		Vector3 rot = moveableTurret.rotation.eulerAngles;
//		float xDiff = Mathf.Abs(lookRotation.x - rot.x);
		float yDiff = Mathf.Abs(lookRotation.y - rot.y);
//		float zDiff = Mathf.Abs(lookRotation.z - rot.z);
		return yDiff < threshold;
	}
}
