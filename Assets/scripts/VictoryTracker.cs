﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryTracker : MonoBehaviour {
	
	public string conquestTag = "EnemyStructure";

	public static VictoryTracker inst;
	
	public GameObject resourceObject;
	private  resource_tracker resourceScript;
	
	private bool checkGasVictory = true;
	private bool checkConquestVictory = true;
	
	public int totalOfGasVents;
	public int ventsControlled;

	void Awake() {
		inst = this;
	}
	
	void Start() {
		UIController.inst.victoryScreen.active = false;
		UIController.inst.gameOverScreen.active = false;
		totalOfGasVents = CountAllGasVents();
		resourceScript = resourceObject.GetComponent<resource_tracker>();
	}
	
	public int CountAllGasVents() {
		int count = 0;
		GameObject[] allTerrain = GameObject.FindGameObjectsWithTag("MapStructure");
		foreach (GameObject obj in allTerrain) {
			if (obj.transform.parent != null) {
				gas_vent vent = obj.transform.parent.gameObject.GetComponent<gas_vent>();
				if (vent != null) {
					count += 1;
				}
			}
		}
		return count;
	}
	
	void Update() {
		CheckForVictory();
	}
	
	public void Defeat() {
		UIController.inst.OpenDefeatUI();
	}
	
	public void Victory() {
		UIController.inst.OpenVictoryUI();
	}
	
	public void CheckForVictory() {
		bool victorious = GetVictoryStatus();
		if(victorious) {
			Victory();
		}
	}
	
	public void PushGasVentVictoryCheck(int numb_vents) {
		checkGasVictory = true;
		ventsControlled = numb_vents;	
	}
	
	public void PushCheckConquestVictory() {
		checkConquestVictory = true;
	}
	
	public bool GetVictoryStatus() {
		return CheckForConquestVictory() || CheckForGasVictory();
	}
	
	public bool CheckForConquestVictory() {
		if(!checkConquestVictory) { return false; }
		checkConquestVictory = false;
		GameObject[] enemiesLeft = GameObject.FindGameObjectsWithTag(conquestTag);
		Debug.Log("check for conquest victory: " + enemiesLeft.Length + " enemies left");
		return enemiesLeft.Length == 0;
	}
	
	public bool CheckForGasVictory() {
		if (!checkGasVictory) { return false; }
		checkGasVictory = false;
		if (totalOfGasVents == 0) {
			totalOfGasVents = CountAllGasVents();
			if (totalOfGasVents == 0) { return false; }
		}
		Debug.Log("check for gas victory: playre controls " + ventsControlled + "/" + totalOfGasVents);
		Debug.Log("check for gas victory: " + (totalOfGasVents <= ventsControlled));
		return totalOfGasVents <= ventsControlled;
	}
	
	
}
