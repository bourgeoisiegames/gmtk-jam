﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class tile {
		public bool has_prefab = true;
   		public Transform prefab = null;
   		public int width = 1;
   		public int height = 1;
   		public float[] transitions;//indexed starting with empty/0, and then all of the tiles in order
   		public float[] mean_biases;
}

public class generate_map : MonoBehaviour {

	public int map_width = 120;
	public int map_height = 120;
	public float tile_offsets = 1.0f;
	public int n_simulation_steps = 10;
	public float p_markov = 0.5f;
	public int mean_kernel_size = 1;

	public int player_safe_radius = 30;

	public Transform player_spawn_prefab;
	public Transform enemy_spawn_prefab;
	public int enemy_spawn_tile_size;
	public int player_spawn_tile_size;
	public int n_enemy_spawns = 3;

	public tile[] tiles;

	private int[,] map;
	private int n_tiles;

	// Use this for initialization
	void Start () {
		n_tiles = tiles.Length;
		map = new int[map_height,map_width];
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				map[i,j] = 0;
			}
		}
		fill_map();
		build_map();

	}

	void build_map(){
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				int map_ind = map[i,j];
				if(map_ind > -1 && tiles[map_ind].has_prefab){
					bool can_place = true;
					int sy = i;
					int ey = i + tiles[map_ind].height;
					int sx = j;
					int ex = j + tiles[map_ind].width;
					if(ex > map_width){ex = map_width;}
					if(ey > map_height){ey = map_height;}
					if(sx < 0){sx = 0;}
					if(sy < 0){sy = 0;}
					for(int y = sy; y < ey; y++){
						for(int x = sx; x < ex; x++){
							can_place = can_place && (map[y,x] == map_ind);
						}
					}
					if(can_place){
						for(int y = sy; y < ey; y++){
							for(int x = sx; x < ex; x++){
								map[y,x] = -1;
							}
						}
						Vector3 new_tile_position = Vector3.zero;
						new_tile_position.x = tile_offsets*(j + ((tiles[map_ind].width)/2.0f));
						new_tile_position.z = tile_offsets*(i + ((tiles[map_ind].height)/2.0f));
						Instantiate(tiles[map_ind].prefab, new_tile_position, Quaternion.identity);
					}
					else{
						map[i,j] = -1;
					}
				}
				else{
					if(map_ind == -3){
						Vector3 new_tile_position = Vector3.zero;
						new_tile_position.x = tile_offsets*(j + (player_spawn_tile_size/2.0f));
						new_tile_position.z = tile_offsets*(i + (player_spawn_tile_size/2.0f));
						Instantiate(player_spawn_prefab, new_tile_position, Quaternion.identity);
						Vector3 my_new_pos = transform.position;
						my_new_pos.x = new_tile_position.x;
						my_new_pos.z = new_tile_position.z;
						transform.position = my_new_pos;
					}
					else if(map_ind == -4){
						Vector3 new_tile_position = Vector3.zero;
						new_tile_position.x = tile_offsets*(j + (enemy_spawn_tile_size/2.0f));
						new_tile_position.z = tile_offsets*(i + (enemy_spawn_tile_size/2.0f));
						Instantiate(enemy_spawn_prefab, new_tile_position, Quaternion.identity);
					}
				}
			}
		}
	}

	void fill_map(){
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				map[i,j] = 0;
			}
		}
		for(int step = 0; step < n_simulation_steps; step++){
			int[,] next_map = new int[map_height,map_width];
			for(int i = 0; i < map_height; i++){
				for(int j = 0; j < map_width; j++){
					if(Random.Range(0.0f,1.0f) < p_markov){
						next_map[i,j] = map_markov(i,j);
					}
					else{
						next_map[i,j] = map_mean(i,j);
					}
				}
			}
			map = next_map;
		}
		if(!mark_playable_area()){
			fill_map();
		}		
	}

	int[] get_random_tile_of_type(int ind){
		int[] ans = new int[2];
		ans[0] = 0;
		ans[1] = 0;

		int counter = 0;
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				if(map[i,j] == ind){
					counter++;
				}
			}
		}
		counter = Random.Range(0,counter);
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				if(map[i,j] == ind){
					if(counter==0){
						ans[0] = i;
						ans[1] = j;
						return ans;
					}
					counter-=1;
				}
			}
		}

		return ans;
	}

	int count_tiles_of_type(int ind){
		int counter = 0;
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				if(map[i,j] == ind){
					counter++;
				}
			}
		}
		return counter;
	}

	float map_distance(int[] a, int[] b){
		Vector2 aa = new Vector2(0.0f + a[0], 0.0f + a[1]);
		Vector2 bb = new Vector2(0.0f + b[0], 0.0f + b[1]);
		return Vector2.Distance(aa,bb);
	}

	int[] get_farthest_tile_of_type(int[] from, int ind){
		int[] ans = new int[2];
		ans[0] = 0;
		ans[1] = 0;

		float farthest = 0.0f;
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				int[] new_pos = new int[2];
				new_pos[0] = i;
				new_pos[1] = j;
				float dis = map_distance(from,new_pos);
				if(map[i,j] == ind && dis > farthest){
					farthest = dis;
					ans[0] = i;
					ans[1] = j;
				}
			}
		}

		return ans;
	}

	float dis_to_nearest_tile_not_of_type(int[] from, int ind){

		float nearest = 1.0f + map_width*map_height;
		bool seen = false;
		for(int i = 0; i < map_height; i++){
			for(int j = 0; j < map_width; j++){
				int[] new_pos = new int[2];
				new_pos[0] = i;
				new_pos[1] = j;
				float dis = map_distance(from,new_pos);
				if(map[i,j] != ind && dis < nearest){
					nearest = dis;
					seen = true;
				}
			}
		}

		if(!seen){
			return -1.0f;
		}
		return nearest;
	}

	bool mark_playable_area(){
		int[] player_spawn_tile = get_random_tile_of_type(0);
		if(dis_to_nearest_tile_not_of_type(player_spawn_tile,0) < 0.5f + player_spawn_tile_size){
			return false;
		}
		int sy = player_spawn_tile[0] - player_safe_radius;
		int ey = player_spawn_tile[0] + player_safe_radius;
		int sx = player_spawn_tile[1] - player_safe_radius;
		int ex = player_spawn_tile[1] + player_safe_radius;
		if(ex > map_width){ex = map_width;}
		if(ey > map_height){ey = map_height;}
		if(sx < 0){sx = 0;}
		if(sy < 0){sy = 0;}
		for(int y = sy; y < ey; y++){
			for(int x = sx; x < ex; x++){
				if(map[y,x] == 0) {map[y,x] = -2;}
			}
		}
		map[player_spawn_tile[0],player_spawn_tile[1]] = -3;

		if(count_tiles_of_type(0) < n_enemy_spawns + 1){
			return false;
		}

		for(int n = 0; n < n_enemy_spawns; n++){
			int[] e_spawn = get_random_tile_of_type(0);
			map[e_spawn[0],e_spawn[1]] = -4;
		}

		int[] known_vent = get_random_tile_of_type(0);
		map[known_vent[0],known_vent[1]] = 2;

		return true;
	}

	int map_mean(int i, int j){
		int sy = i - mean_kernel_size;
		int ey = i + mean_kernel_size + 1;
		int sx = j - mean_kernel_size;
		int ex = j + mean_kernel_size + 1;
		if(ex > map_width){ex = map_width;}
		if(ey > map_height){ey = map_height;}
		if(sx < 0){sx = 0;}
		if(sy < 0){sy = 0;}

		float[] scores = new float[n_tiles];
		for(int sind = 0; sind < n_tiles; sind++){
			scores[sind] = 0.0f;
		}

		for(int y = sy; y < ey; y++){
			for(int x = sx; x < ex; x++){
				scores[map[y,x]] += 1.0f;
			}
		}

		for(int sind = 0; sind < n_tiles; sind++){
			scores[sind] *= tiles[map[i,j]].mean_biases[sind] * Random.Range(0.0f,1.0f);
		}

		int best = 0;
		for(int sind = 0; sind < n_tiles; sind++){
			if(scores[sind] > scores[best]){
				best = sind;
			}
		}
		return best;
	}

	int map_markov(int i, int j){
		int tile_ind = map[i,j];
		float[] scores = new float[n_tiles];
		for(int sind = 0; sind < n_tiles; sind++){
			scores[sind] = tiles[tile_ind].transitions[sind] * Random.Range(0.0f,1.0f);
		}

		int best = 0;
		for(int sind = 0; sind < n_tiles; sind++){
			if(scores[sind] > scores[best]){
				best = sind;
			}
		}
		return best;
	}

	// Update is called once per frame
	void Update () {
		//
	}
}
