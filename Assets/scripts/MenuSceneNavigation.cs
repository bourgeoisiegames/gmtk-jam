﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

public class MenuSceneNavigation : MonoBehaviour {
	
//	public string gameSceneName = "game-scene";
	public static readonly int TITLE_SCREEN = 0;
	public static readonly int GAME_SCENE = 1;
	public static readonly int GAME_OVER = 2;
	public static readonly int VICTORY_SCREEN = 3;

	public void StartGameButton() {
		Debug.Log("starting new Game");
		SceneManager.LoadScene(GAME_SCENE);
	}
	
	public void TitleScreenButton() {
		Debug.Log("starting new Game");
		SceneManager.LoadScene(TITLE_SCREEN);
	}
	
	public void ExitGame() {
//		if (Application.isEditor) {
//			EditorApplication.Exit(0);
//		} else {
			Application.Quit();
//		}
	}
}
