﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class random_model_scale : MonoBehaviour {

	public Vector3 scaling_tolerance;
	public Vector3 rotational_tolerance;

	// Use this for initialization
	void Start () {
		transform.localScale += scaling_tolerance*Random.Range(-1.0f,1.0f);
		transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + rotational_tolerance*Random.Range(-1.0f,1.0f));
	}

}
