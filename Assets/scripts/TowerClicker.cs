﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerClicker : MonoBehaviour {
	
	public string playerGunTag = "PlayerStructure";
	public string enemyStructureTag = "EnemyStructure";
	
	public string playerStructureLayer = "PlayerStructure";
	public string enemyStructureLayer = "EnemyStructure";
	
	public KeyCode multiSelectKey; // = KeyCode.Ctrl;
	public KeyCode deSelectKey; // = KeyCode.Alt;
	
	private List<TowerGun> turretsSelected = new List<TowerGun>();
	
	
	// Update is called once per frame
	void Update () {
		// on left click, handle selecting a tower to attack
		if (Input.GetMouseButtonDown(0))
		{
			GameObject obj = GetClickedTarget();
			if (obj == null) {
				Debug.Log("clicked nothing");
			}
			else if (obj.tag == playerGunTag) {
//				TowerGun gun = TryGettingTowerGun(obj);
				TowerGun gun = TryGettingTowerGunInParent(obj);
				if (MultiSelectKeysPressed()) {
					HandleMultiSelectClick(gun);
				}
				else {
					HandleNormalClick(gun);
				}
			} 
			else { 
				Debug.Log("selected tag (" + obj.tag + ") != " + playerGunTag + " on '" + obj.name + "'"); 
			}
		}
		if (Input.GetMouseButtonDown(1)) {
			GameObject obj = GetClickedTarget();
			if (obj == null) {
				Debug.Log("clicked nothing");
			}
			else if (obj.tag == enemyStructureTag) {
				Debug.Log("clicked enemy struct");
//				DestructableBuilding building = TryGettingDestructibleBuilding(obj);
				DestructableBuilding building = TryGettingDestructibleBuildingInParent(obj);
				foreach (TowerGun gun in turretsSelected) {
//					gun.SetTargetByUI(building.gameObject.transform);
					gun.SetTarget(building.gameObject.transform);
				}
			}
			else {
				Debug.Log("attacking non-enemy tag '" + obj.tag + "' on " + obj.name); 
			}
		}
	}
	
	public TowerGun TryGettingTowerGunInParent(GameObject obj) {
		if (obj == null) { return null; }
		TowerGun gun = obj.GetComponent<TowerGun>();
		if (gun != null) { return gun; }
		
		if(obj.transform.parent != null) {
			return TryGettingTowerGunInParent(obj.transform.parent.gameObject);
		}		
		return null;
	}
	
	public DestructableBuilding TryGettingDestructibleBuildingInParent(GameObject obj) {
		if (obj == null) { return null; }
		DestructableBuilding gun = obj.GetComponent<DestructableBuilding>();
		if (gun != null) { return gun; }
		
		if(obj.transform.parent != null) {
			return TryGettingDestructibleBuildingInParent(obj.transform.parent.gameObject);
		}
		return null;		
	}
	
	public bool MultiSelectKeysPressed() {
		return Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl);
	}
	
	public GameObject GetClickedTarget() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		int layerMask = LayerMask.GetMask(playerStructureLayer, enemyStructureLayer);

//		if(Physics.Raycast(ray, out hit, 100))
		if(Physics.Raycast(ray, out hit, 100, layerMask))
		{
			GameObject clicked = hit.transform.gameObject;
			Debug.Log("clicked on '" + clicked.name + "' on the '" + clicked.layer + "' layer");
			return clicked;
		}
		Debug.Log("Clicked Nothing!");
		return null;
	}
	
	public TowerGun TryGettingTowerGun(GameObject tower) {
		if (tower == null) { return null; }
		TowerGun gun = tower.GetComponent<TowerGun>();
		if (gun != null) { return gun; }
		
		foreach (Transform child in tower.transform) {
			gun = TryGettingTowerGun(child.gameObject);
			if (gun != null) { return gun; }
		}
		return null;
	}
	
	public DestructableBuilding TryGettingDestructibleBuilding(GameObject building) {
		if (building == null) { return null; }
		DestructableBuilding gun = building.GetComponent<DestructableBuilding>();
		if (gun != null) { return gun; }
		
		foreach (Transform child in building.transform) {
			gun = TryGettingDestructibleBuilding(child.gameObject);
			if (gun != null) { return gun; }
		}
		return null;
	}
	
	public void ClearAllSelected() {
		foreach (TowerGun gun in turretsSelected) {
			gun.isSelected = false;
		}
		turretsSelected = new List<TowerGun>();
	}
	
	public void HandleNormalClick(TowerGun gun) {
		ClearAllSelected();
		if(gun != null) {
			gun.isSelected = true;
			turretsSelected.Add(gun);
			Debug.Log("selected one gun");
		}
	}
	
	public void HandleMultiSelectClick(TowerGun gun) {
		if(gun == null) { return; }
		if (turretsSelected.Contains(gun)) {
			gun.isSelected = false;
			turretsSelected.Remove(gun);
			Debug.Log("removed gun '" + gun.gameObject.name + "' from selection");
		}
		else {
			gun.isSelected = true;
			turretsSelected.Add(gun);
			Debug.Log("added gun '" + gun.gameObject.name + "' to selection");
		}
	}
	
//	public void HandleDeselectClick(TowerGun gun) {
//		if(gun != null) {
//			gun.isSelected = false;
//			turretsSelected.Remove(gun);
//			Debug.Log("deselected one gun");
//		}
//	}
}
