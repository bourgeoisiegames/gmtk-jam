﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class select_building_button : MonoBehaviour {

	private resource_tracker tracker;
	public Transform to_build = null;

	// Use this for initialization
	void Start () {
		tracker = (resource_tracker)Camera.main.GetComponent("resource_tracker");
	}

	public void use(){
		Debug.Log(Time.time);
		tracker.set_placing(to_build);
	}

}
