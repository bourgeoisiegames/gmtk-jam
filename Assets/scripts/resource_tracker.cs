﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class resource_tracker : MonoBehaviour {

	public int gas = 0;
	public int max_gas = 200;
	public float update_time = 3.0f;
	public int gas_vent_yield = 5;
	public float mine_range = 20.0f;

	private float next_update;
	private int num_vents;

	public Text gas_meter;

	public GameObject placing = null;

	// Use this for initialization
	void Start () {
		next_update = Time.time + update_time;
	}

	// Update is called once per frame
	void Update () {

		if(Time.time > next_update){
			next_update += update_time;
			mine_gas();
		}

		if(gas > max_gas){
			gas = max_gas;
		}

		gas_meter.text = "Gas: " + gas + "/" + max_gas + " (" + num_vents + ")";

		if(placing != null){
			int cost = placing.GetComponent<placable_building_script>().gas_cost;
			gas_meter.text += "\nCost: " + cost + "\nBalance: " + (gas - cost);
			Vector3 placing_pos = Input.mousePosition;
			placing_pos.z = Camera.main.transform.position.y;

 			placing_pos = Camera.main.ScreenToWorldPoint(placing_pos);
 			placing_pos.y = 0.0f;
 			placing.transform.position = placing_pos;
		}

	}

	void mine_gas () {
		gas_vent[] vents = GameObject.FindObjectsOfType<gas_vent>();
		gas_mine[] mines = GameObject.FindObjectsOfType<gas_mine>();
		num_vents = 0;
		foreach (gas_vent vent in vents){
			bool in_range = false;
			foreach (gas_mine mine in mines){
				if(Vector3.Distance(vent.gameObject.transform.position,mine.gameObject.transform.position) < mine_range){
					in_range = true;
					break;
				}
			}
			if(in_range){
				gas += gas_vent_yield;
				num_vents += 1;
			}

		}
		VictoryTracker.inst.PushGasVentVictoryCheck(num_vents);
	}

	public void set_placing(Transform prefab){
		if(placing != null){
			clear_placing();
		}
		placing = Instantiate(prefab, Vector3.zero, Quaternion.identity).gameObject;
	}

	public void clear_placing(){
		if(placing != null){
			Destroy(placing);
		}
		placing = null;
	}

}
