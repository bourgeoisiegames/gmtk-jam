﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGun : MonoBehaviour {

	public Transform launchSpot;
	public GameObject projectilePrefab;
	public float launchMagnitude = 1000.0f;
	public bool attacking = true;
	public string targetType = GlobalVariables.ENEMY_STRUCTURE;
	public string attackType = GlobalVariables.PLAYER_SHOT;
	public float damage = 40.0f;
	public GameObject selectedIndicator;
	public int attackLayer = 9;
	public List<string> ignoredTargets;
	
	public Transform currentTarget;
	public float reloadTime = 1.0f;
	public float maxRange = 20.0f;
	// number of seconds added to the bullet expiration time to make sure it reaches at LEAST max range
	public float bulletExpirationFudgeFactor = .01f; 
	private float _untilNextShot = 1.0f;
	
	private bool _selected;
	public bool isSelected {
		get { return _selected; }
		set {
			if (selectedIndicator != null) {
				selectedIndicator.active = value;
			}
			_selected = value;
			SetTargetedIndicatorActive(currentTarget, value);
		}
	}
	
	void Start() {
		isSelected = false;
	}
	
	void Update() {
//		Debug.Log("hey listen");
		CheckRange();
		if (_untilNextShot > 0) {
			_untilNextShot -= Time.deltaTime;
		}
		if (currentTarget == null) { 
			PickNewTarget();
		} else if(attacking && _untilNextShot <= 0) {
			FireProjectileAt(currentTarget);
			_untilNextShot += reloadTime;
		}
	}
	
	// gets distance to target; if target is further than the max range, set target to null;
	// also, return true if a target is still selected at the end of the method execution (ie. target is valid and in range) else return false;
	public bool CheckRange() {
		if (currentTarget == null) { return false; }
		if (!TargetIsInRange(currentTarget)) {
			Debug.Log("'" + gameObject.name + "' cancelled its attack on target '" + currentTarget.gameObject.name + "' because it's outside maxRange");
			Debug.Log("range: " + GetDistanceToTarget(currentTarget) + " > max Range: " + maxRange); 
			SetTarget(null);
			return false;
		}
		return true;
	}
	
	public void PickNewTarget() {
		GameObject[] objs = GameObject.FindGameObjectsWithTag(targetType);
		float distanceToTarget = float.MaxValue;
		SetTarget(null);
		for (int i = 0; i < objs.Length; i++) {
			DestructableBuilding building = objs[i].GetComponent<DestructableBuilding>();
			if (building == null) { continue; }
			float dist = GetDistanceToTarget(building.transform);
			if (dist < distanceToTarget) {
				distanceToTarget = dist;
				SetTarget(building.transform);
			}
		}
		CheckRange();
	}
	
	public void SetTargetedIndicatorActive(Transform target, bool active) {
		if (target == null) { return; }
		Transform indicator = target.Find("TargetedIndicator");
		if (indicator != null) {
			indicator.gameObject.active = (active && isSelected);
		}
	}
	
	public bool SetTargetByUI(Transform newTarget) {
		bool success = SetTarget(newTarget);
		if (!success) {
			RangeWarningCtrl.inst.ShowMessage();
		}
		return success;
	}
	
	public bool SetTarget(Transform newTarget) {
		SetTargetedIndicatorActive(currentTarget, false);
		if (newTarget == null) {
//			Debug.Log("new target is null");
			currentTarget = null;
			return false;
		}
		if (TargetIsInRange(newTarget)) {
			currentTarget = newTarget;
			//Debug.Log("set new target");
			SetTargetedIndicatorActive(currentTarget, true);
			return true;
		}
//		Debug.Log("Turret '" + gameObject.name + "' could not lock on target '" + newTarget.gameObject.name + "' b/c it is out of range at " + Mathf.Round(GetDistanceToTarget(newTarget)) + " and max range is " + maxRange);
		currentTarget = null;
		return false;
		
	}
	
	public bool TargetIsInRange(Transform target) {
		if (target == null) { return false; }
		float dist = GetDistanceToTarget(target);
//		Debug.Log("dist(" + Mathf.Round(dist) + ") < range (" + maxRange + ") ==> " + (dist < maxRange));
		return dist < maxRange;
	}
	
	public void FireProjectileAt(Transform target) {
		if (target == null) { return; }
		GameObject projectile =  GetProjectile(projectilePrefab);
		Rigidbody rb = projectile.GetComponent<Rigidbody>();
		Vector3 v = GetLaunchVector(target);
		rb.AddForce(v);
	}
	
	public GameObject GetProjectile(GameObject prefab) {
		GameObject projectile = Instantiate<GameObject>(projectilePrefab);
		projectile.tag = attackType;
		projectile.layer = attackLayer;
		foreach (Transform child in projectile.transform) {
			child.gameObject.layer = attackLayer;
		}
		projectile.transform.position = launchSpot.position;
		FaceTarget(projectile, currentTarget);
		SetupProjectileScript(projectile);
		return projectile;
	}
	
	private void FaceTarget(GameObject bullet, Transform target) {
		Vector3 v = GetVectorToTarget(target);
		Vector3 direction = Vector3.RotateTowards(bullet.transform.forward, v, float.MaxValue, 0.0f);
		bullet.transform.rotation = Quaternion.LookRotation(direction);
	}
	
	private void SetupProjectileScript(GameObject projectile) {
		BulletScript bullet = projectile.GetComponent<BulletScript>();
		bullet.timeLeft = GetTimeToProjectileExpiration();
		bullet.damage = damage;
		bullet.ignoredTargets = this.ignoredTargets;
	}
	
	private float GetTimeToProjectileExpiration() {
		return maxRange / launchMagnitude;
	}
	
	public Vector3 GetLaunchVector(Transform target) {
		return GetVectorToTarget(target).normalized * launchMagnitude;
	}
	
	public Vector3 GetVectorToTarget(Transform target) {
		if (target == null) {
			return new Vector3(0, 0, 0);
		}
		float x = target.position.x - launchSpot.position.x;
		float y = target.position.y - launchSpot.position.y;
		float z = target.position.z - launchSpot.position.z;
		return new Vector3(x, y, z);
	}
	
	public float GetDistanceToTarget(Transform target) {
		if (target == null) { return float.MaxValue; }
		Vector3 v = GetVectorToTarget(target);
		return v.magnitude;
	}
}
