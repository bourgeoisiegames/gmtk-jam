﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moss_spread_script : MonoBehaviour {

	public Transform to_build = null;
	public float build_radius = 1.2f;

	public float min_spawn_distance = 3.0f;
	public float max_spawn_distance = 5.0f;

	public int max_clutter = 4;

	public float minx = 0.0f;
	public float miny = 0.0f;
	public float maxx = 120.0f;
	public float maxy = 120.0f;

	private string[] non_buildable_tags = new string[]{"PlayerStructure","EnemyStructure","MapStructure"};

	private resource_tracker tracker;
	private bool unblocked;
	private bool can_place;
	private bool not_crowded;
	private bool in_range;

	public float spread_time = 6.0f;
	public float spread_chance = 0.05f;
	private float timer;

	// Use this for initialization
	void Start () {
		tracker = (resource_tracker)Camera.main.GetComponent("resource_tracker");
		can_place = false;
		unblocked = true;
		in_range = false;
		not_crowded = true;
		timer = Time.time + spread_time;
	}

	// Update is called once per frame
	void Update () {
		if(Time.time > timer){
			timer = Time.time + spread_time;
			if(Random.Range(0.0f,1.0f) < spread_chance){

				Vector3 proposed_position = Vector3.zero;
				Vector2 rcircle = (Random.insideUnitCircle * Random.Range(min_spawn_distance,max_spawn_distance));
				proposed_position.x += rcircle.x;
				proposed_position.z += rcircle.y;
				proposed_position.y = 0.0f;
				proposed_position += transform.position;

				unblocked = true;
				Collider[] hitColliders = Physics.OverlapSphere(proposed_position, build_radius);
				foreach(Collider c in hitColliders){
					unblocked = unblocked && !block_construction(c.gameObject);
				}

				in_range = true;
				if(proposed_position.x < minx || proposed_position.x > maxx){
					in_range = false;
				}
				if(proposed_position.z < miny || proposed_position.z > maxy){
					in_range = false;
				}

				int crowd = 0;
				moss_thing[] mosses = GameObject.FindObjectsOfType<moss_thing>();
				foreach(moss_thing moss in mosses){
					if(Vector3.Distance(proposed_position, moss.gameObject.transform.position) < min_spawn_distance){
						crowd++;
					}
				}
				not_crowded = crowd < max_clutter;

				can_place = unblocked && in_range && not_crowded;

				Debug.Log("DEADWOOD:  "+can_place+","+not_crowded+","+in_range+","+unblocked+","+transform.position+","+proposed_position);

				if(can_place){
					Instantiate(to_build, proposed_position, Quaternion.identity);
				}

			}
		}

	}

    bool block_construction(GameObject g){
    	foreach(string s in non_buildable_tags){
    		if(s.Equals(g.tag)){
    			return true;
    		}
    	}
    	return false;
    }

}
