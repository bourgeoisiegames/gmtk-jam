﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour {
	
	public float maxShield = 3000.0f;
	public float restartThreshold = 750.0f;
	public float currentShield;

	public float amountCharged = 1.0f;
	public float chargeIncrement = 1.0f;
	public GameObject shieldBubble;
	
	private float _untilNextCharge = 0f;
	
	void Start() {
		currentShield = maxShield;
		if (shieldBubble == null) {
			Transform trans = transform.Find("ShieldBubble");
			if (trans != null) {
				shieldBubble = trans.gameObject;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (currentShield >= maxShield) { return; }
		if (_untilNextCharge <= 0) {
			_untilNextCharge += chargeIncrement;
			currentShield += amountCharged;
			if (currentShield >= maxShield) {
				currentShield = maxShield;
			}
			if (currentShield >= restartThreshold) {
				RaiseShield();
			}
		}
		_untilNextCharge -= Time.deltaTime;
	}
	
	public float DamageShield(float damage) {
		if (damage >= currentShield) {
			float remainingDamage = damage - currentShield;
			currentShield = 0;
			DropShield();
			return remainingDamage;
		} else {
			currentShield -= damage;
			return 0;
		}
	}
	
	private void DropShield() {
		shieldBubble.active = false;
	}
	
	private void RaiseShield() {
		shieldBubble.active = true;
	}
}
