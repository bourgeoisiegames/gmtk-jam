﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableBuilding : MonoBehaviour, BuildingI {

	public float maxHp;
	public GameObject onDeathPrefab;
	
	void Start() {
		_hp = maxHp;
	}
	
	private float _hp;
	
	public float DEBUG_HP;
	
	public float currentHp {
		get {
			return _hp;
		}
		set {
			if (value > maxHp) {
				Debug.Log("capping HP at max");
				_hp = maxHp;
			}
			else if (value <= 0) {
				BecomeDead();
			}
			else {
//				Debug.Log("set hp to " + _hp + " on " + gameObject.name);
				_hp = value;
			}
			DEBUG_HP = _hp;
		}
	}
	
	public void DealDamage(float damage) {
//		Debug.Log("Dealing " + damage + " damage");
		currentHp -= damage;
	}

	public void BecomeDead() {
		// TODO 
		Debug.Log("Becoming dead");
		if (onDeathPrefab != null) {
			GameObject explosion = Instantiate<GameObject>(onDeathPrefab);
			explosion.transform.position = gameObject.transform.position;
		}
		Destroy(gameObject);
		VictoryTracker.inst.PushCheckConquestVictory();
	}
	
}
