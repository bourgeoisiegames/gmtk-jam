﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_pan_script : MonoBehaviour {

	public double pan_threshold = 0.01;
	public float viewport_speed = 4.0f;
	public float zoom_speed = 4.0f;
	public float min_height = 8.0f;
	public float max_height = 32.0f;

	public float minx = 10.0f;
	public float miny = 10.0f;
	public float maxx = 110.0f;
	public float maxy = 110.0f;

	public float strain_distance = 25.0f;

	private Vector3 viewport_velocity;
	private double x_border_size;
	private double y_border_size;

	// Use this for initialization
	void Start () {
		viewport_velocity = Vector3.zero;
		x_border_size = pan_threshold*Screen.width;
		y_border_size = pan_threshold*Screen.height;
	}

	// Update is called once per frame
	void Update () {

		viewport_velocity = Vector3.zero;

		double x = Input.mousePosition.x;
		double y = Input.mousePosition.y;
		
		if(x > Screen.width - x_border_size){
			viewport_velocity.x = 1.0f;
		}
		if(x < x_border_size){
			viewport_velocity.x = -1.0f;
		}
		if(y > Screen.height - y_border_size){
			viewport_velocity.z = 1.0f;
		}
		if(y < y_border_size){
			viewport_velocity.z = -1.0f;
		}

		viewport_velocity = viewport_velocity.normalized;

		viewport_velocity.y = -Input.GetAxis("Mouse ScrollWheel")*zoom_speed;

		if(viewport_velocity.magnitude > 0){

			Vector3 projected = transform.position + (viewport_velocity*viewport_speed);

			if(projected.y < min_height || projected.y > max_height){
				projected.y = transform.position.y;
			}

			if(projected.x < minx || projected.x > maxx){
				projected.x = transform.position.x;
			}

			if(projected.z < miny || projected.z > maxy){
				projected.z = transform.position.z;
			}

			power_station[] stations = GameObject.FindObjectsOfType<power_station>();
			bool in_range = false;
			//bool in_range2 = false;
			power_station mindis = null;
			float mini = 10000.0f;
			float dis = 0.0f;
			//float dis2 = 0.0f;
			foreach(power_station station in stations){
				//dis = Vector3.Distance(transform.position, station.gameObject.transform.position);
				dis = Vector3.Distance(projected, station.gameObject.transform.position);
				//in_range = in_range || (dis < station.range);
				if(dis < mini){
					mini = dis;
					mindis = station;
				}
			}

			if(dis > mindis.range){
				Vector3 projected2 = projected - mindis.gameObject.transform.position;
				projected2 = projected2.normalized;
				projected2 = projected2*mindis.range;
				projected2 += mindis.gameObject.transform.position;
				projected2.y = transform.position.y;
				projected = projected2;

			}
			transform.position = projected;


			//in_range2 = dis > strain_distance;

			/*if(in_range){
				transform.position = projected;
			}
			else if(in_range2){
				Vector3 newpos2 = mindis.position;
				newpos2.y = transform.position.y;
				transform.position = newpos2;
			}*/
		}

	}
}
