﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables {

	public static readonly string ENEMY_STRUCTURE = "EnemyStructure";
	public static readonly string PLAYER_STRUCTURE = "PlayerStructure";
	public static readonly string ENEMY_SHOT = "EnemyAttack";
	public static readonly string PLAYER_SHOT = "PlayerAttack";
}
