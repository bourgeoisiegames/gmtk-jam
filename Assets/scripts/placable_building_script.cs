﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placable_building_script : MonoBehaviour {

	public Transform to_build = null;
	public int gas_cost = 150;
	public float build_radius = 1.2f;

	public float minx = 0.0f;
	public float miny = 0.0f;
	public float maxx = 120.0f;
	public float maxy = 120.0f;

	private string[] non_buildable_tags = new string[]{"PlayerStructure","EnemyStructure","MapStructure"};

	private resource_tracker tracker;
	private bool unblocked;
	private bool can_afford;
	private bool can_place;
	private bool in_range;

	// Use this for initialization
	void Start () {
		tracker = (resource_tracker)Camera.main.GetComponent("resource_tracker");
		can_place = false;
		unblocked = true;
		can_afford = false;
		in_range = false;
	}

	// Update is called once per frame
	void Update () {
		unblocked = true;
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, build_radius);
		foreach(Collider c in hitColliders){
			unblocked = unblocked && !block_construction(c.gameObject);
		}

		in_range = false;
		power_station[] stations = GameObject.FindObjectsOfType<power_station>();
		foreach(power_station station in stations){
			in_range = in_range || Vector3.Distance(transform.position, station.gameObject.transform.position) < station.range;
		}

		if(transform.position.x < minx || transform.position.x > maxx){
			in_range = false;
		}

		if(transform.position.z < miny || transform.position.z > maxy){
			in_range = false;
		}

		can_afford = (tracker.gas >= gas_cost);

		can_place = unblocked && can_afford && in_range;

		if(Input.GetMouseButtonUp(0)){
			if(can_place){
        		Instantiate(to_build, transform.position, Quaternion.identity);
        		tracker.gas -= gas_cost;
        		tracker.clear_placing();
        	}
		}

		if(Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2)){
			tracker.clear_placing();
		}

		if(can_place){
			foreach(Renderer r in GetComponentsInChildren<Renderer>()){
    			r.material.color= Color.green;
 			}
		}
		else{
			foreach(Renderer r in GetComponentsInChildren<Renderer>()){
    			r.material.color= Color.red;
 			}
		}

	}

    bool block_construction(GameObject g){
    	foreach(string s in non_buildable_tags){
    		if(s.Equals(g.tag)){
    			return true;
    		}
    	}
    	return false;
    }

}
