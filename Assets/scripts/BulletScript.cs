﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	
	public float damage = 40.0f;
	public float timeout = 15.0f;
	public float timeLeft;
	public List<string> ignoredTargets;
	public GameObject impactEffect;
	
	void Start() {
		timeLeft = timeout;
	}
	
	void Update() {
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0) {
			Expire();
		}
	
	}
	
	private void Expire() {
//		Debug.Log("expiring attack of type " + gameObject.tag);
		Destroy(gameObject);
	}
	
	private void Expire(GameObject obj) {
//		Debug.Log("expiring attack of type '" + gameObject.tag + "' on collision with '" + obj.tag + "' object " + obj.name);
		Destroy(gameObject);
	}
	
	void OnTriggerEnter(Collider other) {
		OnHit(other.gameObject);
	}
	
	void OnHit(GameObject obj) {
//		Debug.Log("a " + gameObject.tag + " collided with a " + obj.tag + " '" + obj.name + "'");
		if (ignoredTargets.Contains(obj.tag)) {
			// do not expire if target tag ignored;
//			Debug.Log(gameObject.tag + " ignoring collision with " + obj.tag);
			return;
		}
		if (obj.tag.Equals("Shield")) {
//			Debug.Log("hit shield");
			ShieldController sCtrl = FindShieldController(obj);
			damage = sCtrl.DamageShield(damage);
			if (damage <= 0) {
				Impact(obj);
			}
		}
		else {
			DestructableBuilding building = FindDamagedStructure(obj);
//			Debug.Log("building found " + (building != null));
			if (building != null) { 
	//			Debug.Log(gameObject.tag + " dealt " + damage + " damage to a " + obj.tag);
				building.DealDamage(damage);
			}
			Impact(obj);
		}
	}
	
	public void Impact(GameObject hit) {
		GameObject effect = Instantiate<GameObject>(impactEffect);
		effect.transform.position = transform.position;
		Expire(hit);
	}
	
	private ShieldController FindShieldController(GameObject obj) {
		ShieldController shield = obj.GetComponent<ShieldController>();
		if (shield != null) {
			return shield;
		}
		if(obj.transform.parent != null) {
			return FindShieldController(obj.transform.parent.gameObject);
		}
		return null;
	}
	
	private DestructableBuilding FindDamagedStructure(GameObject obj) {
		DestructableBuilding building = obj.GetComponent<DestructableBuilding>();
		if (building != null) {
			return building;
		}
		if(obj.transform.parent != null) {
			return FindDamagedStructure(obj.transform.parent.gameObject);
		}
		return null;
	}
}
