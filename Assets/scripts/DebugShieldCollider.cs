﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugShieldCollider : MonoBehaviour {

	void OnCollisionEnter(Collision other) {
		Debug.Log("Collided with shield generator");
	}
}
